from flask import Flask
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)

data = [{"client_id": 1, "client_key": "CLIENT01", "client_secret": "SECRET01", "status": True},
    {"client_id": 2, "client_key": "CLIENT02", "client_secret": "SECRET01","status": False},
    {"client_id": 3, "client_key": "CLIENT03","client_secret": "SECRET03","status": False},
    {"client_id": 4, "client_key": "CLIENT04", "client_secret": "SECRET04", "status": False}]



class Client(Resource):
    def get(self, client_id = None):
        if client_id == None:
            return data
        elif len(data)>= client_id and client_id>0:
            for i in range(len(data)):
                if client_id == i+1:
                    return data[i]
        else:
            return {'message':'NOT_FOUND'}, 404

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('client_id', type=int, help='ID Client must be Integer', location='json', required=True)
        parser.add_argument('client_secret', type=str, help='Client secret must be string', location='json', required= True)
        parser.add_argument('client_key', type=str, help='Client key must be string', location='json', required= True)
        parser.add_argument('status', type=bool, help='status must be true or false', location='json', required= False)
        args = parser.parse_args()
        for i in data:
             if i["client_id"] == args["client_id"]:
                return {"message": "Client id has been exist"}, 444
        
        data.append(args)
        return data, 201

    def put(self,client_id):
        parser = reqparse.RequestParser()
        parser.add_argument('client_id', type=int, help='ID Client must be Integer', location='json', required=True)
        parser.add_argument('client_key', type=str, help='Client key must be string', location='json', required= True)
        parser.add_argument('client_secret', type=str, help='Client secret must be string', location='json', required= True)
        parser.add_argument('status', type=bool, help='status must be true or false', location='json', required= True)
        args = parser.parse_args()

        if client_id == args["client_id"]:
            for i in data:
                if i["client_id"] == args["client_id"]:
                    i["client_secret"] = args["client_secret"]
                    i["client_key"] = args["client_key"]
                    i["status"] = args["status"]
                    return i    
        else:
            return {"message": "ID cannot be change"}, 444
                
            
        return {"message": "NOT FOUND"}, 404

                
    def delete(self, client_id):
        for i in data:
            if i["client_id"]== client_id:
                # indeks = data.index(i)
                data.remove(i)
                return data
            
        return {'message':'DELETED'}


api.add_resource(Client,'/client', '/client/<int:client_id>')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)